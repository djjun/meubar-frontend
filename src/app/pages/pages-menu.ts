import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'ion-ios-home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Cardápio',
    icon: 'ion-ios-book-outline',
    link: '/pages/cardapio',
    home: true,
  },
  {
    title: 'Mesas',
    icon: 'ion-beer',
    link: '/pages/mesa',
    home: true,
  },
  {
    title: 'Auth',
    icon: 'nb-locked',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },
];
