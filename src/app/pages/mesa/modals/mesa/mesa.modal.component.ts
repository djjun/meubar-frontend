import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response } from '@angular/http';

import { Mesa } from '../../../../@core/model/mesa.model';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'ngx-modal',
  templateUrl: './mesa.modal.component.html',
})
export class ModalMesaComponent {

  private mesa: Mesa;

  constructor(
    private activeModal: NgbActiveModal,
    private http: Http
  ) {
    this.mesa = new Mesa({});
  }

  save() {
    this.http.post('/mesa/add', this.mesa).subscribe((value) => {
      this.closeModal(this.mesa);
    });
  }

  closeModal(data) {
    this.activeModal.close(data);
  }

}