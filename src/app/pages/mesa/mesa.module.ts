import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { MesaComponent } from './mesa.component';


import { ModalMesaComponent } from './modals/mesa/mesa.modal.component';


@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    MesaComponent,
    ModalMesaComponent
  ],
  entryComponents: [
    ModalMesaComponent,
  ],
})
export class MesaModule { }
