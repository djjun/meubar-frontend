import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Mesa } from '../../@core/model/mesa.model';

import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalMesaComponent } from './modals/mesa/mesa.modal.component';

@Component({
  selector: 'ngx-mesa',
  templateUrl: './mesa.component.html'
})
export class MesaComponent implements OnInit {
  
  
  public mesas$:Observable<Mesa[]>;
  public mesasSub: any;
  total: number;
  private activeModal: any;

  constructor(
    private http: Http,
    private modalService: NgbModal
  ) {
   
  }

  ngOnInit() {
    this.loadMesas();
  }

  ngOnDestroy() {
    if (this.mesasSub) {
      this.mesasSub.unsubscribe();
    }
  }

  loadMesas() {
    this.mesas$ = this.http.get('/mesa/all')
      .pipe(
        map((res: Response) => {
          let json = res.json();
          let array = new Array();
          for (let m of json) {
            array.push(new Mesa(m));
          }
          return array;
        }),
        catchError((error) => { 
          return of(error);
        })
      );
    this.mesasSub = this.mesas$.subscribe((value) => { 
      this.total = value ? value.length : 0
    });
  }

  excluir(mesa: Mesa) {
    console.log("excluir" + mesa.uuid);
    this.http.get('/mesa/delete/' + mesa.uuid)
      .pipe()
      .subscribe((value) => this.loadMesas());
  }

  showLargeModal() {
    this.activeModal = this.modalService.open(ModalMesaComponent, { size: 'sm', container: 'nb-layout' });

    this.activeModal.componentInstance.modalHeader = 'Large Modal';
    this.activeModal.result.then((result) => {
      if (result) {
        this.loadMesas();
      }
    }, (reason) => {
    });

  }

}
