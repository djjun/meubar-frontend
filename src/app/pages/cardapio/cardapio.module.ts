import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { CardapioComponent } from './cardapio.component';


import { ModalProdutoComponent } from './modals/produto/produto.component';
import { ModalCategoriaComponent } from './modals/categoria/categoria.component';


@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    CardapioComponent,
    ModalProdutoComponent,
    ModalCategoriaComponent
  ],
  entryComponents: [
    ModalProdutoComponent,
    ModalCategoriaComponent
  ],
})
export class CardapioModule { }
