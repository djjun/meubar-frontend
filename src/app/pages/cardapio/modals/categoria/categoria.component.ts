import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response } from '@angular/http';

import { Categoria } from '../../../../@core/model/categoria.model';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'ngx-modal',
  templateUrl: './categoria.component.html',
})
export class ModalCategoriaComponent {

  private categorias: any = []
  private categoria: Categoria;

  constructor(
    private activeModal: NgbActiveModal,
    private http: Http
  ) {
    this.categoria = new Categoria({});
  }

  save() {
    this.http.post('/categoria/add', this.categoria).subscribe((value) => {
      this.closeModal(this.categoria);
    });
  }

  closeModal(data) {
    this.activeModal.close(data);
  }

}