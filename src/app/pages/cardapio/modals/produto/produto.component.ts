import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response } from '@angular/http';

import { Produto } from '../../../../@core/model/produto.model';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'ngx-modal',
  templateUrl: './produto.component.html',
})
export class ModalProdutoComponent {

  private categorias: any = []
  private produto: Produto;

  constructor(
    private activeModal: NgbActiveModal,
    private http: Http
  ) {
    this.produto = new Produto({});
    let cardapio = this.http.get('/categoria/all')
      .pipe(
        map((res: Response) => res.json()),
        catchError(() => of('Error!'))
      );

    cardapio.subscribe( (data) => {
      this.categorias = data;
      console.log(this.categorias);
    });
  }

  loadProduto(produto: Produto) {
    if (produto) {
      this.produto = produto;
    }
  }

  changeListener($event) : void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.produto.foto = myReader.result;
    }
    myReader.readAsDataURL(file);
  }

  save() {
    let url = '/produto/add';
    if (this.produto.uuid) {
      url = '/produto/' + this.produto.uuid;
    }
    this.http.post(url, this.produto).subscribe((value) => {
      this.closeModal(this.produto);
    });
  }

  closeModal(data) {
    this.activeModal.close(data);
  }

}