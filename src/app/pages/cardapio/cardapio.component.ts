import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';


import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';

import { Produto } from '../../@core/model/produto.model';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalProdutoComponent } from './modals/produto/produto.component';
import { ModalCategoriaComponent } from './modals/categoria/categoria.component';

@Component({
  selector: 'ngx-cardapio',
  templateUrl: './cardapio.component.html'
})
export class CardapioComponent implements OnInit {
  
  public produtos$:Observable<Produto[]>;
  public produtosSub: any;
  total: number;
  private activeModal: any;

  constructor(
    private http: Http,
    private modalService: NgbModal
  ) {
   
  }

  ngOnInit() {
    this.loadProdutos();
  }

  ngOnDestroy() {
    if (this.produtosSub) {
      this.produtosSub.unsubscribe();
    }
  }

  loadProdutos() {
    this.produtos$ = this.http.get('/produto/all')
      .pipe(
        map((res: Response) => {
          let json = res.json();
          let array = new Array();
          for (let p of json) {
            array.push(new Produto(p));
          }
          return array;
        }),
        catchError((error) => { 
          return of(error);
        })
      );
    this.produtosSub = this.produtos$.subscribe((value) => { 
      this.total = value ? value.length : 0
    });
  }

  editar(produto: Produto) {
    this.showModalProduto(produto);
  }

  excluir(produto: Produto) {
    console.log("excluir" + produto.uuid);
    this.http.get('/produto/delete/' + produto.uuid)
      .pipe()
      .subscribe((value) => this.loadProdutos());
  }

  showModalProduto(produto?: Produto) {
    this.activeModal = this.modalService.open(ModalProdutoComponent, { size: 'lg', container: 'nb-layout' });

    (<ModalProdutoComponent>this.activeModal.componentInstance).loadProduto(produto);
    this.activeModal.componentInstance.modalHeader = 'Large Modal';
    this.activeModal.result.then((result) => {
      if (result) {
        this.loadProdutos();
      }
    }, (reason) => {
    });

  }

  showModalCategoria() {
    this.activeModal = this.modalService.open(ModalCategoriaComponent, { size: 'sm', container: 'nb-layout' });

    this.activeModal.componentInstance.modalHeader = 'Large Modal';
    this.activeModal.result.then((result) => {
    }, (reason) => {
    });

  }

}
