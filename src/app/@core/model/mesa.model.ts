export class Mesa {
	id: number;
	uuid: string;
	nome: string;

  	constructor (data: any = {}) {
  		this.id = data.id;
	    this.uuid = data.uuid;
	    this.nome = data.nome;
  	}
}
