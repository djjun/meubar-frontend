import { Categoria } from './categoria.model';

export class Produto {
  uuid: string;
  id: number;
  nome: string;
  descricao: string;
  codBarras: string;
  valor: number;
  precoPromocao: number;
  categoria: Categoria;
  foto: string;

  constructor (data: any = {}) {
    this.uuid = data.uuid;
    this.id = data.id;
    this.descricao = data.descricao;
    this.nome = data.nome;
    this.valor = data.valor;
    this.foto = data.foto;
    this.precoPromocao = data.precoPromocao;
    this.categoria = new Categoria(data.categoria);
  }
}
