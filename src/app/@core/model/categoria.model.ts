export class Categoria {
  uuid: string;
  nome: string;

  constructor (data: any = {}) {
    this.uuid = data.uuid;
    this.nome = data.nome;
  }
}
