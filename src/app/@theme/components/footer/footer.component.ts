import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"><b>Barto</b> 2018</span>
  `,
})
export class FooterComponent {
}
